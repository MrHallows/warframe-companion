/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo.nightwave

data class NightwavePojo(
        val activation: String,
        val active: Boolean,
        val activeChallenges: List<ActiveChallenge>,
        val expiry: String,
        val id: String,
        val params: Params,
        val phase: Int,
        val possibleChallenges: List<Any>,
        val rewardTypes: List<String>,
        val season: Int,
        val startString: String,
        val tag: String
)