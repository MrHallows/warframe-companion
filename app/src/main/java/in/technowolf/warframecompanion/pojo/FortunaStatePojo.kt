/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class FortunaStatePojo(
        val activation: String,
        val expiry: String,
        val id: String,
        val isWarm: Boolean,
        val shortString: String,
        val state: String,
        val timeLeft: String
)