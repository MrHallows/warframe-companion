/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class FissuresStatePojo(
        val activation: String,
        val active: Boolean,
        val enemy: String,
        val eta: String,
        val expired: Boolean,
        val expiry: String,
        val id: String,
        val missionType: String,
        val node: String,
        val startString: String,
        val tier: String,
        val tierNum: Int
)