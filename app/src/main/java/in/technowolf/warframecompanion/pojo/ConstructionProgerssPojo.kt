/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class ConstructionProgerssPojo(
        val fomorianProgress: String,
        val id: String,
        val razorbackProgress: String,
        val unknownProgress: String
)