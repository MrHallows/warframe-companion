/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class SortieVariant(
        val boss: String,
        val missionType: String,
        val modifier: String,
        val modifierDescription: String,
        val node: String,
        val planet: String
)