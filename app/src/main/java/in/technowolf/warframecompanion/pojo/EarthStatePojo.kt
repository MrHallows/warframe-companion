/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class EarthStatePojo(
        val activation: String,
        val expiry: String,
        val id: String,
        val isDay: Boolean,
        val state: String,
        val timeLeft: String
)