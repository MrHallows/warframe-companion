/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo.nightwave

data class ActiveChallenge(
        val activation: String,
        val active: Boolean,
        val desc: String,
        val expiry: String,
        val id: String,
        val isDaily: Boolean,
        val isElite: Boolean,
        val reputation: Int,
        val startString: String,
        val title: String
)