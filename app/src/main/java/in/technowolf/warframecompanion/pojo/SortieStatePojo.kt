/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class SortieStatePojo(
        val activation: String,
        val active: Boolean,
        val boss: String,
        val eta: String,
        val expired: Boolean,
        val expiry: String,
        val faction: String,
        val id: String,
        val rewardPool: String,
        val startString: String,
        val variants: List<SortieVariant>
)