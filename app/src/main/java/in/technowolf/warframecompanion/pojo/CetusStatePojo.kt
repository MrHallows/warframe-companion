/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.pojo

data class CetusStatePojo(
        val activation: Long,
        val expiry: String,
        val id: String,
        val isCetus: Boolean,
        val isDay: Boolean,
        val shortString: String,
        val state: String,
        val timeLeft: String
)