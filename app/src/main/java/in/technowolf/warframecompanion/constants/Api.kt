/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.constants

const val CETUS_TRACKER = "https://api.warframestat.us/pc/cetusCycle"
const val FORTUNA_TRACKER = "https://api.warframestat.us/pc/vallisCycle"
const val EARTH_TRACKER = "https://api.warframestat.us/pc/earthCycle"
const val FISSURES_TRACKER = "https://api.warframestat.us/pc/fissures"
const val SORTIE_TRACKER = "https://api.warframestat.us/pc/sortie"
const val CONSTRUCTION_PROGRESS_TRACKER = "https://api.warframestat.us/pc/constructionProgress"
const val NIGHTWAVE_TRACKER = "https://api.warframestat.us/pc/nightwave"