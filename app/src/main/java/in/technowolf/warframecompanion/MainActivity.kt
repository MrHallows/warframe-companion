/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion

import `in`.technowolf.warframecompanion.fragments.*
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var dashboard = Dashboard()
    private var worldTracker = WorldTracker()
    private var fissures = Fissures()
    private var invasion = Invasion()
    private var sortie = Sortie()
    private var nightwave = Nightwave()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        loadFragment(dashboard)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        return when (item.itemId) {
//            R.id.action_settings -> {
//                return true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }
//    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.nav_dashboard -> {
                loadFragment(dashboard)
            }
            R.id.nav_world_tracker -> {
                loadFragment(worldTracker)
            }
            R.id.nav_fissures -> {
                loadFragment(fissures)
            }
//            R.id.nav_invasions -> {
//                //loadFragment(invasion)
//                Toast.makeText(applicationContext,
//                        "Feature not Developed Yet!", Toast.LENGTH_SHORT).show()
//            }
            R.id.nav_sortie -> {
                loadFragment(sortie)
            }
            R.id.nav_nightwave -> {
                loadFragment(nightwave)
            }
            R.id.nav_about -> {
                startActivity(Intent(applicationContext, AboutActivity::class.java))
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.activity_fragment, fragment)
                .commit()
    }

}
