/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.adapters

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.pojo.nightwave.ActiveChallenge
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class NightwaveChallengesAdapter(private var challengesList: List<ActiveChallenge>) : RecyclerView.Adapter<NightwaveChallengesAdapter.ChallengesViewHolder>() {

    class ChallengesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtChallengeTitle = view.findViewById(R.id.txt_challenge_title) as TextView
        var txtChallengeDesc = view.findViewById(R.id.txt_challenge_description) as TextView
        var tatReputation = view.findViewById(R.id.txt_challenge_reputation) as TextView
        var txtChallengeType = view.findViewById(R.id.txt_challenge_type) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChallengesViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.nightwave_challenge_row, parent, false)
        return ChallengesViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return challengesList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ChallengesViewHolder, position: Int) {
        val challenge = challengesList[position]
        holder.txtChallengeTitle.text = (position + 1).toString() + ". " + challenge.title
        holder.txtChallengeDesc.text = challenge.desc
        holder.tatReputation.text = challenge.reputation.toString() + " Reputation"
        when {
            challenge.isDaily -> holder.txtChallengeType.text = "Daily Task"
            challenge.isElite -> holder.txtChallengeType.text = "Elite Weekly Task"
            else -> {
                holder.txtChallengeType.text = "Weekly Task"
            }
        }
    }


}