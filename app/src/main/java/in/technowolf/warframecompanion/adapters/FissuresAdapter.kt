/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.adapters

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.adapters.FissuresAdapter.FissuresViewHolder
import `in`.technowolf.warframecompanion.pojo.FissuresStatePojo
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class FissuresAdapter(fissuresList: List<FissuresStatePojo>) : RecyclerView.Adapter<FissuresViewHolder>() {
    private var fissureList = fissuresList

    class FissuresViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtFissureNode: TextView = view.findViewById(R.id.txt_fissure_node) as TextView
        var txtFissureNodeType: TextView = view.findViewById(R.id.txt_fissure_node_type) as TextView
        var txtFissureTier: TextView = view.findViewById(R.id.txt_fissure_tier) as TextView
        var txtFissureEta: TextView = view.findViewById(R.id.txt_fissure_eta) as TextView
        var fissureEnemyFactionImage: ImageView = view.findViewById(R.id.fissure_enemy_faction) as ImageView
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FissuresViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.fissures_list_row, parent, false)

        return FissuresViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return fissureList.size
    }

    override fun onBindViewHolder(holder: FissuresViewHolder, position: Int) {
        val fissure: FissuresStatePojo = fissureList[position]
        if (!fissure.expired) {
            holder.txtFissureNode.text = (fissure.node)
            holder.txtFissureNodeType.text = (fissure.missionType) + " - " + (fissure.enemy)
            holder.txtFissureTier.text = (fissure.tier)
            holder.txtFissureEta.text = (fissure.eta) + " to expire"
        }
    }


}