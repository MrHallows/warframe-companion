/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.fragments


import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.constants.SORTIE_TRACKER
import `in`.technowolf.warframecompanion.pojo.SortieStatePojo
import `in`.technowolf.warframecompanion.tools.HttpHandler
import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson

private val TAG = Sortie::class.java.simpleName

class Sortie : Fragment() {
    private var sortieData: String? = ""
    private var isFirstRun = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.v(TAG, "At Sortie Fragment")
        val rootView = inflater.inflate(R.layout.fragment_sortie, container, false)

        if (isFirstRun) {
            GetSortie(object : GetSortie.AsyncResponse {
                override fun processFinish(output: String?) {
                    sortieData = output
                    populateUI(rootView, output)
                    isFirstRun = false
                }
            }).execute()
        } else {
            populateUI(rootView, sortieData)
        }
        return rootView
    }

    @SuppressLint("SetTextI18n")
    fun populateUI(rootView: View, sortieJsonString: String?) {
        val sortieLayout = rootView.findViewById<View>(R.id.layout_sortie)
        val progressBar = rootView.findViewById<View>(R.id.sortie_progress)
        val progressText = rootView.findViewById<TextView>(R.id.txt_sortie_progress_text)

        if (sortieJsonString.isNullOrBlank()) {
            progressBar.visibility = View.GONE
            progressText.text = getString(R.string.server_issue)
            Toast.makeText(rootView.context,
                    getString(R.string.server_issue), Toast.LENGTH_SHORT).show()
        } else {
            val gson = Gson()
            val sortieData: SortieStatePojo =
                    gson.fromJson(sortieJsonString, SortieStatePojo::class.java)
            sortieLayout.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            progressText.visibility = View.GONE
            val sortieBoss = rootView.findViewById<TextView>(R.id.txt_sortie_boss)
            sortieBoss.text = "Defeat " + sortieData.boss

            val sortieTimeLeft =
                    rootView.findViewById<TextView>(R.id.txt_sortie_time_left)
            sortieTimeLeft.text = sortieData.eta + " left"

            val sortieEnemyFaction =
                    rootView.findViewById<TextView>(R.id.txt_sortie_faction)
            sortieEnemyFaction.text = "Faction: " + sortieData.faction

            val sortieNode0 = rootView.findViewById<TextView>(R.id.txt_sortie_node0)
            sortieNode0.text = sortieData.variants[0].node + " Level 50-60"
            val sortieNode1 = rootView.findViewById<TextView>(R.id.txt_sortie_node1)
            sortieNode1.text = sortieData.variants[1].node + " Level 65-80"
            val sortieNode2 = rootView.findViewById<TextView>(R.id.txt_sortie_node2)
            sortieNode2.text = sortieData.variants[2].node + " Level 80-100"

            val sortieMissionType0 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_mission_type0)
            sortieMissionType0.text = "Mission: " + sortieData.variants[0].missionType
            val sortieMissionType1 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_mission_type1)
            sortieMissionType1.text = "Mission: " + sortieData.variants[1].missionType
            val sortieMissionType2 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_mission_type2)
            sortieMissionType2.text = "Mission: " + sortieData.variants[2].missionType

            val sortieCondition0 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_condition0)
            sortieCondition0.text = sortieData.variants[0].modifier
            val sortieCondition1 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_condition1)
            sortieCondition1.text = sortieData.variants[1].modifier
            val sortieCondition2 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_condition2)
            sortieCondition2.text = sortieData.variants[2].modifier

            val sortieConditionDesc0 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_condition_description0)
            sortieConditionDesc0.text = sortieData.variants[0].modifierDescription
            val sortieConditionDesc1 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_condition_description1)
            sortieConditionDesc1.text = sortieData.variants[1].modifierDescription
            val sortieConditionDesc2 =
                    rootView.findViewById<TextView>(R.id.txt_sortie_condition_description2)
            sortieConditionDesc2.text = sortieData.variants[2].modifierDescription
        }
    }

    class GetSortie(param: AsyncResponse) : AsyncTask<Void, Void, String?>() {

        interface AsyncResponse {
            fun processFinish(output: String?)
        }

        private var delegate: AsyncResponse = param

        override fun doInBackground(vararg params: Void?): String? {
            val sh = HttpHandler()
            return sh.makeServiceCall(SORTIE_TRACKER)
        }

        override fun onPostExecute(result: String?) {
            delegate.processFinish(result)
            super.onPostExecute(result)
        }

    }

}
