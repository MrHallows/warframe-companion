/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.fragments


import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.adapters.NightwaveChallengesAdapter
import `in`.technowolf.warframecompanion.constants.NIGHTWAVE_TRACKER
import `in`.technowolf.warframecompanion.pojo.nightwave.NightwavePojo
import `in`.technowolf.warframecompanion.tools.HttpHandler
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*


private val TAG = Nightwave::class.java.simpleName

class Nightwave : Fragment() {
    var nightwaveData: String? = ""
    var isFirstRun = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_nightwave, container, false)

        if (isFirstRun) {
            GetNightwave(object : GetNightwave.AsyncResponse {
                override fun processFinish(output: String?) {
                    nightwaveData = output
                    populateUI(rootView, nightwaveData)
                    isFirstRun = false
                }
            }).execute()

        } else {
            populateUI(rootView, nightwaveData)
        }

        return rootView
    }

    private fun populateUI(rootView: View, nightwaveDataString: String?) {
        val nightwaveLayoutContent = rootView.findViewById<View>(R.id.layout_nightwave_content)
        val progressBar = rootView.findViewById<View>(R.id.progressbar_nightwave_loading)
        val progressText = rootView.findViewById<TextView>(R.id.txt_nightwave_progress_text)

        val nightwaveTitle = rootView.findViewById<TextView>(R.id.txt_nightwave_title)
        val nightwaveExpiration = rootView.findViewById<TextView>(R.id.txt_nightwave_expiration)

        if (nightwaveDataString.isNullOrBlank()) {
            nightwaveExpiration.text = getString(R.string.server_issue)
            Toast.makeText(rootView.context,
                    getString(R.string.server_issue), Toast.LENGTH_SHORT).show()
        } else {
            val gson = Gson()
            val nightwaveData: NightwavePojo = gson.fromJson(nightwaveDataString, NightwavePojo::class.java)
            val challenges = nightwaveData.activeChallenges

            nightwaveTitle.append(nightwaveData.season.toString())
            nightwaveExpiration.append(formatDate(nightwaveData.expiry))

            if (challenges.isEmpty()) {
                Toast.makeText(rootView.context,
                        getString(R.string.server_issue), Toast.LENGTH_SHORT).show()
            } else {
                nightwaveLayoutContent.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                progressText.visibility = View.GONE

                val recyclerView: RecyclerView = rootView.findViewById(R.id.nightwave_recycler_view)
                val challengesAdapter = NightwaveChallengesAdapter(challenges)

                val layoutManager = LinearLayoutManager(rootView.context)
                recyclerView.layoutManager = (layoutManager)
                recyclerView.itemAnimator = DefaultItemAnimator()
                recyclerView.adapter = challengesAdapter
            }
        }

    }

    private fun formatDate(dateString: String): String {
        val inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        val outputFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyy", Locale.ENGLISH)
        val date = LocalDate.parse(dateString, inputFormatter)
        return outputFormatter.format(date)
    }

    class GetNightwave(param: AsyncResponse) : AsyncTask<Void, Void, String?>() {

        interface AsyncResponse {
            fun processFinish(output: String?)
        }

        private var delegate: AsyncResponse = param

        override fun doInBackground(vararg params: Void?): String? {
            val sh = HttpHandler()
            return sh.makeServiceCall(NIGHTWAVE_TRACKER)
        }

        override fun onPostExecute(result: String?) {
            delegate.processFinish(result)
            super.onPostExecute(result)
        }

    }

}
