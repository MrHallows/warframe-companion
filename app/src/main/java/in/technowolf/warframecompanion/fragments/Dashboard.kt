/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.fragments


import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.constants.CONSTRUCTION_PROGRESS_TRACKER
import `in`.technowolf.warframecompanion.pojo.ConstructionProgerssPojo
import `in`.technowolf.warframecompanion.tools.HttpHandler
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson

private val TAG = Dashboard::class.java.simpleName

class Dashboard : Fragment() {
    private var constructionJson: String? = ""
    private var isFirstRun = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.v(TAG, "At Dashboard Fragment")
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_dashboard, container, false)

        if (isFirstRun) {
            GetConstructionProgress(object : GetConstructionProgress.AsyncResponse {
                override fun processFinish(output: String?) {
                    constructionJson = output
                    populateView(rootView, constructionJson)
                    isFirstRun = false
                }
            }).execute()
        } else {
            populateView(rootView, constructionJson)
        }

        return rootView
    }

    fun populateView(rootView: View, constructionJsonString: String?) {
        val gson = Gson()
        val constructionProgressData = gson.fromJson(constructionJsonString, ConstructionProgerssPojo::class.java)

        val progressLayout = rootView.findViewById<View>(R.id.layout_build_progress)
        val loadingProgressBar = rootView.findViewById<ProgressBar>(R.id.progressbar_dashboard_loading)
        val loadingProgressText = rootView.findViewById<TextView>(R.id.txt_dashboard_progress_text)

        progressLayout.visibility = View.VISIBLE
        loadingProgressBar.visibility = View.GONE
        loadingProgressText.visibility = View.GONE

        val fomorianTextView = rootView.findViewById<TextView>(R.id.txt_fomorian_progress)
        val razorbackTextView = rootView.findViewById<TextView>(R.id.txt_razorback_progress)

        if (constructionProgressData != null) {

            var fomorianProgress = constructionProgressData.fomorianProgress.toFloat().toInt()
            var razorbackProgress = constructionProgressData.razorbackProgress.toFloat().toInt()

            if (fomorianProgress > 100) {
                fomorianProgress = 100
            }
            if (razorbackProgress > 100) {
                razorbackProgress = 100
            }

            val fomorianProgressBar = rootView.findViewById<ProgressBar>(R.id.progressbar_fomorian)
            fomorianProgressBar.progress = fomorianProgress

            val razorbackProgressBar = rootView.findViewById<ProgressBar>(R.id.progressbar_razorback)
            razorbackProgressBar.progress = razorbackProgress

            fomorianTextView.append("$fomorianProgress%")
            razorbackTextView.append("$razorbackProgress%")
        } else {
            fomorianTextView.append("Error!")
            razorbackTextView.append("Error!")

            Toast.makeText(rootView.context,
                    getString(R.string.server_issue), Toast.LENGTH_SHORT).show()
        }
    }

}

class GetConstructionProgress(param: AsyncResponse) : AsyncTask<Void, Void, String?>() {

    interface AsyncResponse {
        fun processFinish(output: String?)
    }

    private var delegate: AsyncResponse = param

    override fun doInBackground(vararg params: Void?): String? {
        val sh = HttpHandler()
        return sh.makeServiceCall(CONSTRUCTION_PROGRESS_TRACKER)
    }

    override fun onPostExecute(result: String?) {
        delegate.processFinish(result)
        super.onPostExecute(result)
    }

}
