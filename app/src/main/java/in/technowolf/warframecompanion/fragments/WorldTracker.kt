/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.fragments


import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.constants.CETUS_TRACKER
import `in`.technowolf.warframecompanion.constants.EARTH_TRACKER
import `in`.technowolf.warframecompanion.constants.FORTUNA_TRACKER
import `in`.technowolf.warframecompanion.pojo.CetusStatePojo
import `in`.technowolf.warframecompanion.pojo.EarthStatePojo
import `in`.technowolf.warframecompanion.pojo.FortunaStatePojo
import `in`.technowolf.warframecompanion.tools.HttpHandler
import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson


private val TAG = WorldTracker::class.java.simpleName

class WorldTracker : Fragment() {

    var worldStateJsonList = ArrayList<String?>()
    var isFirstRun = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_world_state, container, false)

        Log.v(TAG, "At WorldTracker Fragment")

        if (isFirstRun) {
            GetWorldStatus(object : GetWorldStatus.AsyncResponse {

                override fun processFinish(output: ArrayList<String?>) {
                    worldStateJsonList = output
                    populateUI(rootView, worldStateJsonList)
                    isFirstRun = false
                }
            }).execute()
        } else {
            populateUI(rootView, worldStateJsonList)
        }
        return rootView
    }

    @SuppressLint("SetTextI18n")
    fun populateUI(rootView: View, trackerList: ArrayList<String?>) {
        val gson = Gson()
        if (trackerList[0].isNullOrBlank() or trackerList[1].isNullOrBlank() or trackerList[2].isNullOrBlank()) {
            Toast.makeText(rootView.context,
                    "Servers facing difficulties, Please try again later.", Toast.LENGTH_SHORT).show()
        } else {
            val cetusState: CetusStatePojo = gson.fromJson(trackerList[0], CetusStatePojo::class.java)
            val fortunaState: FortunaStatePojo = gson.fromJson(trackerList[1], FortunaStatePojo::class.java)
            val earthState: EarthStatePojo = gson.fromJson(trackerList[2], EarthStatePojo::class.java)

            val stateCetus = if (cetusState.state == "day") {
                "Day"
            } else {
                "Night"
            }
            val timeLeftCetus = cetusState.shortString

            val txtIsDayCetus = rootView.findViewById<TextView>(R.id.txt_is_day)
            txtIsDayCetus.text = "Currently it is $stateCetus at Cetus"
            val txtTimeLeftCetus = rootView.findViewById<TextView>(R.id.txt_cetus_night)
            txtTimeLeftCetus.text = timeLeftCetus

            val stateFortuna = if (fortunaState.state == "warm") {
                "Warm"
            } else {
                "Cold"
            }
            val timeLeftFortuna = fortunaState.shortString

            val txtIsDayFortuna = rootView.findViewById<TextView>(R.id.txt_is_warm)
            txtIsDayFortuna.text = "Currently it is $stateFortuna at Fortuna"
            val txtTimeLeftFortuna = rootView.findViewById<TextView>(R.id.txt_fortuna_weather)
            txtTimeLeftFortuna.text = timeLeftFortuna

            val stateEarth = if (earthState.state == "day") {
                "Day"
            } else {
                "Night"
            }
            val isDayEarth = earthState.isDay
            val timeLeftEarth = if (isDayEarth) {
                "${earthState.timeLeft} left for Night"
            } else {
                "${earthState.timeLeft} left for Day"
            }

            val txtIsDayEarth = rootView.findViewById<TextView>(R.id.txt_is_day_earth)
            txtIsDayEarth.text = "Currently it is $stateEarth at Earth"
            val txtTimeLeftEarth = rootView.findViewById<TextView>(R.id.txt_earth_night)
            txtTimeLeftEarth.text = timeLeftEarth
        }
    }

    class GetWorldStatus(param: AsyncResponse) : AsyncTask<Void, Void, ArrayList<String?>>() {

        interface AsyncResponse {
            fun processFinish(output: ArrayList<String?>)
        }

        private var delegate: AsyncResponse = param

        override fun doInBackground(vararg params: Void?): ArrayList<String?> {

            val sh = HttpHandler()
            val cetus = sh.makeServiceCall(CETUS_TRACKER)
            val fortuna = sh.makeServiceCall(FORTUNA_TRACKER)
            val earth = sh.makeServiceCall(EARTH_TRACKER)

            val stateArray = ArrayList<String?>()
            stateArray.add(cetus)
            stateArray.add(fortuna)
            stateArray.add(earth)

            return stateArray
        }

        override fun onPostExecute(result: ArrayList<String?>) {
            delegate.processFinish(result)
            super.onPostExecute(result)
        }
    }

}
