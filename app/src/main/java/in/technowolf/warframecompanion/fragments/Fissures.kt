/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion.fragments


import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.adapters.FissuresAdapter
import `in`.technowolf.warframecompanion.constants.FISSURES_TRACKER
import `in`.technowolf.warframecompanion.fragments.Fissures.GetFissures.AsyncResponse
import `in`.technowolf.warframecompanion.pojo.FissuresStatePojo
import `in`.technowolf.warframecompanion.tools.HttpHandler
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson


private val TAG = Fissures::class.java.simpleName

class Fissures : Fragment() {

    var fissuresJson: String? = ""
    var isFirstRun = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.v(TAG, "At Fissures Fragment")
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_fissures, container, false)

        if (isFirstRun) {
            GetFissures(object : AsyncResponse {
                override fun processFinish(output: String?) {
                    fissuresJson = output
                    populateUI(rootView, fissuresJson)
                    isFirstRun = false
                }
            }).execute()
        } else {
            populateUI(rootView, fissuresJson)
        }

        return rootView
    }

    fun populateUI(rootView: View, fissuresJsonString: String?) {
        val gson = Gson()
        val fissuresLayoutContent = rootView.findViewById<View>(R.id.layout_fissure_content)
        val progressBar = rootView.findViewById<View>(R.id.progressbar_fissures_loading)
        val progressText = rootView.findViewById<TextView>(R.id.txt_fissures_progress_text)

        val fissuresData: Array<FissuresStatePojo> =
                gson.fromJson(fissuresJsonString, Array<FissuresStatePojo>::class.java)

        val arrayList = ArrayList<FissuresStatePojo>()
        arrayList.addAll(fissuresData)
        arrayList.sortWith(Comparator { o1, o2 -> o1.tierNum.compareTo(o2.tierNum) })

        if (arrayList.isNotEmpty()) {
            fissuresLayoutContent.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            progressText.visibility = View.GONE

            val fissuresList: ArrayList<FissuresStatePojo> = ArrayList()
            val recyclerView = rootView.findViewById(R.id.fissures_recycler_view) as RecyclerView
            val fissuresAdapter = FissuresAdapter(fissuresList)

            val mLayoutManager = LinearLayoutManager(rootView.context)
            recyclerView.layoutManager = mLayoutManager
            recyclerView.itemAnimator = DefaultItemAnimator()
            recyclerView.adapter = fissuresAdapter

            fissuresList.addAll(arrayList)
            fissuresAdapter.notifyDataSetChanged()
        } else {
            Toast.makeText(rootView.context,
                    getString(R.string.server_issue), Toast.LENGTH_SHORT).show()
            Log.d(TAG, "ArrayList empty, Something's wrong!")
        }
    }

    class GetFissures(param: AsyncResponse) : AsyncTask<Void, Void, String?>() {

        // you may separate this or combined to caller class.
        interface AsyncResponse {
            fun processFinish(output: String?)
        }

        private var delegate: AsyncResponse = param

        override fun doInBackground(vararg params: Void?): String? {
            val sh = HttpHandler()
            return sh.makeServiceCall(FISSURES_TRACKER)
        }

        override fun onPostExecute(result: String?) {
            delegate.processFinish(result)
            super.onPostExecute(result)
        }
    }


}
