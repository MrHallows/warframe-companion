/*
 * Copyright (c) 2019 TechnoWolf FOSS
 * Provided under terms of MIT License.
 * Do NOT remove this excerpt
 */

package `in`.technowolf.warframecompanion

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar


class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        val toolbar = findViewById<Toolbar>(R.id.toolbar_about)
        toolbar.title = "About"
        setSupportActionBar(toolbar)

        val versionCode = findViewById<TextView>(R.id.txt_about_version_code)
        versionCode.text = BuildConfig.VERSION_NAME

    }

    fun donate(v: View) {
        startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.paypal.me/daksh7011")))
    }

    fun gitlabRepo(v: View) {
        startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("https://gitlab.com/technowolf/warframe-companion")))
    }

    fun opensourceLicenses(v: View) {
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.dialog_opensource_licenses, null)
        val textView = view.findViewById(R.id.about_dialog_textview) as TextView
        textView.text = getString(R.string.opensource_license_texts)
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Opensource Library Licenses")
        alertDialog.setView(view)
        alertDialog.setPositiveButton("OK", null)
        val alert = alertDialog.create()
        alert.show()
    }

    fun teamAndContributors(v: View) {
        Toast.makeText(applicationContext, "Will be added in next update!", Toast.LENGTH_SHORT).show()
    }

    fun developer(v: View) {
        startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("https://gitlab.com/daksh7011")))
    }
}

