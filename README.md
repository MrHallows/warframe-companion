<div align="center">
    <h1>
        <br>
        <a href="#">
            <img src="https://gitlab.com/technowolf/warframe-companion/raw/426a8fc83612c35b0277a1283247394d4b680e1b/images/artwork.png" 
            alt="Warframe-Companion Logo" width="200"></a>
        <br>
        Warframe Companion
        <br>
    </h1>
    <h4 align="center">Warframe Companion is AD-Free impmementaion for Warframe with everything a Tenno will need.<br />
    Proudly Open-Source and AD-Free forever.</h4>
</div>

<div align="center">
    <a href="https://www.gnu.org/licenses/gpl-3.0.html" target="_blank">
        <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg">
    </a>
    <a href="http://makeapullrequest.com" target="_blank">
        <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat">
    </a>
    <a href="https://gitlab.com/technowolf/triple20/commits/master">
        <img alt="pipeline status" src="https://gitlab.com/technowolf/warframe-companion/badges/master/pipeline.svg" />
    </a>
    </a>
    <a href="https://saythanks.io/to/daksh7011" target="_blank">
        <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
    </a>
    <a href="https://www.paypal.me/daksh7011" target="_blank">
        <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
    </a>
    <br>
    <br>
</div>

# Overview
Open-Source companion for Warframe to provide players all the information at one place.

Features:

- World Time Tracker
- Ongoing Fissures and Invasions
- Sortie Tracker
- Mods, Enemies, Blueprint Drop Tables
- Missions Drop Tables
- Cetus Bounties
- Solaris United Bounties
- Derelict, Arbitration, Nightmare Drop Tables
- AD Free Forever because Captain Vor can't be Destroyed

and Much more...

All these features are being constantly updated and 100% synced with the game.

Warframe Companion is not an official Warframe app. All rights reserved to Warframe and Digital Extremes.

# Contribution Guide
Please take a look at the [contributing](CONTRIBUTING.md) guidelines if you're interested in helping by any means.

Contribution to this project is not limited to coding help, You can suggest a feature, help with docs, UI design 
ideas or even some typos. You are just an issue away. Don't hesitate to create an issue.

# Emailware

Triple20 is an emailware. Which means, if you liked using this app or has helped you in anyway, I'd like you send me an email 
on [daksh@technowolf.in](mailto:daksh@technowolf.in) about anything you'd want to say about this software. 
I'd really appreciate it! Plus I would be more than happy to know my initiative helped someone. :)


# License

[MIT License](LICENSE) Copyright (c) 2019 TechnoWolf FOSS

 Warframe-Companion is provided under terms of MIT license.

# Links

[Issue Tracker](https://gitlab.com/technowolf/triple20/issues)


[![Powered by Warframe Community Developers](https://warframestat.us/wfcd.png)](https://github.com/WFCD "Powered by Warframe Community Developers")